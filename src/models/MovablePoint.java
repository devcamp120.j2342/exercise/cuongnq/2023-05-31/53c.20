package models;

import interfaces.Movable;

public class MovablePoint implements Movable {
    private int x;
    private int y;

    public MovablePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "MovablePoint[x="+this.x+",y="+this.y+"]";
    }

    @Override
    public void moveUp() {
        this.y ++;
    }   
    
    @Override
    public void moveDown() {
        this.y --;
    }

    @Override
    public void moveLeft() {
        this.x --;
    }

    @Override
    public void moveRight() {
        this.x ++;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
